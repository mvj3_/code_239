package com.myslidingdrawer.android;

import android.app.Activity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SlidingDrawer;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;

public class TestSlidingDrawer extends Activity
{
	/** Called when the activity is first created. */
	private SlidingDrawer sd;
	private GridView gv;
	private ImageView iv;
	private int[] itemIcons = new int[]
	{ R.drawable.alarm, R.drawable.calendar, R.drawable.camera, R.drawable.clock, R.drawable.music, R.drawable.tv };
	private String[] itemString =  new String[]{"Alarm","Calendar","camera","clock","music","tv"};
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.slidrawer);
		/* 初始化对象 */
		sd = (SlidingDrawer) findViewById(R.id.drawer1);
		gv = (GridView) findViewById(R.id.mycontent1);
		iv = (ImageView) findViewById(R.id.myImage);
		/* 使用告定义的MyGridViewAdapter设置GridView里面的item内容 */
		GridAdapter ga = new GridAdapter(TestSlidingDrawer.this, itemString, itemIcons);
		gv.setAdapter(ga);
		 /* 设定SlidingDrawer被打开的事件处理 */
		sd.setOnDrawerOpenListener(new OnDrawerOpenListener()
		{

			public void onDrawerOpened()
			{
				iv.setImageResource(R.drawable.close);

			}
		});
		 /* 设定SlidingDrawer被关闭的事件处理 */
		sd.setOnDrawerCloseListener(new OnDrawerCloseListener()
		{

			public void onDrawerClosed()
			{
				iv.setImageResource(R.drawable.open);
			}
		});
	}
}